Welcome to the CAMWP DEV starter package!

We use this starter package to initiate any project developed at CAMWP DEV.
Before starting the development, check the following criterias:

1. HTML Markup
==============

- Located in the html/ folder
- The default package is already shipped with Twitter Bootrap 3 SASS
- There is a flag in the config.rb file which you need to comment out
the development output style and uncomment the production one when the 
site goes live
- There is gitingore configuration line at wp-content/themes/ThemeName/.sass-cache/*
which you need to replace the ThemeName by your current theme (project name)
- Run commpass watch in the root of the html/ folder
- Happy styling

2. WordPress Integration
========================

- There is a composer.json file that contains all the configuration to install
the fresh wordpress for your project
- Add more plugins or themes you wish to bulk install or update the current 
required plugins by changing its version number to the latest ones
- Run the composer install
- Create a database in your local machine
- Update the wp-config.php configuration option to match your database name,
database username, and the database password
- When you need to deploy the site to staging, you will need to update the variable
int the staging WP_ENV condition by updating the project folder name and the
database credentials