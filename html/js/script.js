
 
$( function() {

	var PROJECTNAME = {		
		init: function() {
			
			this.backgroundImage();
			
		},
		
		backgroundImage: function() {
			let imageUrl = $( '.has-bg' ).attr( 'data-bg-image' );

			$( '.has-bg' ).css('background-image', 'url(' + imageUrl + ')')
		}
		
	}
	
	$( document ).ready( function() {
		
		PROJECTNAME.init();
		
	});
	
});